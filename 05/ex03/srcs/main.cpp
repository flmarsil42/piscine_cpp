#include "../includes/Bureaucrat.hpp"
#include "../includes/Form.hpp"
#include "../includes/ShrubberyCreationForm.hpp"
#include "../includes/PresidentialPardonForm.hpp"
#include "../includes/RobotomyRequestForm.hpp"
#include "../includes/Intern.hpp"

int main()
{
    Intern Xavier;
    Bureaucrat Michel("mich", 1);
    Form *form;
    try
    {
        form = Xavier.MakeForm("RobotomyRequestForm", "mike");
        std::cout << *form;
        delete form;
        std::cout << "-----" << std::endl;
        
        form = Xavier.MakeForm("PresidentialPardonForm", "mike");
        std::cout << *form;
        delete form;
        std::cout << "-----" << std::endl;
        
        form = Xavier.MakeForm("ShrubberyCreationForm", "mike");
        std::cout << *form;
        Michel.signForm(*form);
        Michel.executeForm(*form);
        delete form;
        std::cout << "-----" << std::endl;

        form = Xavier.MakeForm("random", "mike");
        
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    std::cout << "-----" << std::endl;
}