#include "../includes/Form.hpp"

/* Constructor */
Form::Form(const std::string& name, int gradeForSign, int gradeForExec, std::string target)
    : _name(name), _is_signed(false), _gradeForSign(gradeForSign), _gradeForExec(gradeForExec),
        _target(target)
{
    if (_gradeForSign < 1 || _gradeForExec < 1)
        throw Form::GradeTooHighException();
    if (_gradeForSign > 150 || _gradeForExec > 150)
        throw Form::GradeTooLowException();
}

/* Destructor */
Form::~Form() {}

/* Copy constructor */
Form::Form(const Form& copyObj)
    :   _name(copyObj._name),
        _is_signed(copyObj._is_signed),
        _gradeForSign(copyObj._gradeForSign),
        _gradeForExec(copyObj._gradeForExec)
{}

/* Overloading operator = << */
Form& Form::operator = (const Form& assignObj)
{
    this->_is_signed = assignObj._is_signed;
    return (*this);
}

std::ostream& operator << (std::ostream& flux, const Form& form)
{
    flux
        << "Form : " << form.getName() << std::endl
        << "Grade for sign : " << form.getGradeForSign() << std::endl
        << "Grade for exec : " << form.getGradeForExec() << std::endl
        << "Signed : ";
        if (form.getSigned() == true)
            flux << "Yes" << std::endl;
        else
            flux << "Not yet" << std::endl;
    return (flux);
}

/* Methods */
std::string Form::getName() const
{
    return (this->_name);
}

int Form::getGradeForSign() const
{
    return (this->_gradeForSign);
}

int Form::getGradeForExec() const
{
    return (this->_gradeForExec);
}

bool Form::getSigned() const
{
    return (this->_is_signed);
}

std::string Form::getTarget() const
{
    return (this->_target);
}

void Form::beSigned(const Bureaucrat& bureaucrat)
{
    int grade = bureaucrat.getGrade();

    if (grade > this->_gradeForSign)
        throw Form::GradeTooLowException();
    else
        this->_is_signed = true;
}

void Form::execute(Bureaucrat const & executor) const
{
    if (executor.getGrade() > this->getGradeForExec())
        throw Form::GradeTooLowException();
    else if (this->_is_signed == false)
        throw Form::NotSignedException();
}

/* Exceptions */
Form::GradeTooHighException::GradeTooHighException() throw() {}

const char* Form::GradeTooHighException::what() const throw()
{
    return ("Form::GradeTooHigh exception found\n");
}
Form::GradeTooLowException::GradeTooLowException() throw() {}

const char* Form::GradeTooLowException::what() const throw()
{
    return ("Form::GradeTooLow exception found\n");
}

Form::NotSignedException::NotSignedException() throw() {}

const char* Form::NotSignedException::what() const throw()
{
    return ("Form::NotSignedException exception found\n");
}
