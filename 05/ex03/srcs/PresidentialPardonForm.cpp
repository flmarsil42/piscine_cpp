#include "../includes/PresidentialPardonForm.hpp"

/* Constructor */
PresidentialPardonForm::PresidentialPardonForm(const std::string& target)
    : Form("PresidentialPardon", 25, 5, target) {}

/* Destructor */
PresidentialPardonForm::~PresidentialPardonForm() {}

/* Copy constructor */
PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm& copyObj)
    :   Form(copyObj) {}

/* Overloading operator */
PresidentialPardonForm& PresidentialPardonForm::operator = (const PresidentialPardonForm& assignObj)
{
    this->Form::operator=(assignObj);
    return(*this);
}

/* Methods */
void PresidentialPardonForm::execute(Bureaucrat const & executor) const
{
    this->Form::execute(executor); // check authorizations first

    std::cout 
        << "\033[34m"
        << this->getTarget()
        << " has been forgiven by Zafod Beeblebrox\033[m"
        << std::endl;
}