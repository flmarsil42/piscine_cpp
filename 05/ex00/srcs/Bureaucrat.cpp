#include "../includes/Bureaucrat.hpp"

/* Constructor */
Bureaucrat::Bureaucrat(std::string const name, int grade)
    : _name(name)
{
    if (grade > 150)
        throw Bureaucrat::GradeTooLowException();
    else if (grade < 1)
        throw Bureaucrat::GradeTooHighException();
    this->_grade = grade; 
}

/* Destructor */
Bureaucrat::~Bureaucrat() {}

/* Copy constructor */
Bureaucrat::Bureaucrat(const Bureaucrat& copyObj)
    : _name(copyObj.getName()), _grade(copyObj.getGrade()) {}

/* Overloading oprator */
Bureaucrat& Bureaucrat::operator = (const Bureaucrat& assignObj)
{
    this->_name = assignObj.getName();
    this->_grade = assignObj.getGrade();
    return (*this);
}

std::ostream& operator << (std::ostream& flux, const Bureaucrat& bureaucrat)
{
    flux 
        << bureaucrat.getName() 
        << ", bureaucrat grade "
        << bureaucrat.getGrade()
        << std::endl;
    return (flux);
}

/* Methods */
const std::string& Bureaucrat::getName() const
{
    return (this->_name);
}

int Bureaucrat::getGrade() const
{
    return (this->_grade);
}

void Bureaucrat::upGrade()
{
    if ((this->getGrade() - 1) < 1)
        throw Bureaucrat::GradeTooHighException();
    else
        this->_grade--;
}

void Bureaucrat::downGrade()
{
    if ((this->getGrade() + 1) > 150)
        throw Bureaucrat::GradeTooLowException();
    else
        this->_grade++;
}

/* Exceptions */

Bureaucrat::GradeTooHighException::GradeTooHighException() throw() {}

const char* Bureaucrat::GradeTooHighException::what() const throw()
{
	return ("Grade is too high (max upgrade : 1)\n");
}

Bureaucrat::GradeTooLowException::GradeTooLowException() throw() {}

const char* Bureaucrat::GradeTooLowException::what() const throw()
{
	return ("Grade is too low (max downgrade : 150)\n");
}
