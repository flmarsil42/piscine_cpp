#include "../includes/RobotomyRequestForm.hpp"

/* Constructor */
RobotomyRequestForm::RobotomyRequestForm(const std::string& target)
    : Form("RobotomyRequest", 72, 45, target) {}

/* Destructor */
RobotomyRequestForm::~RobotomyRequestForm() {}

/* Copy constructor */
RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm& copyObj)
    :   Form(copyObj) {}

/* Overloading operator */
RobotomyRequestForm& RobotomyRequestForm::operator = (const RobotomyRequestForm& assignObj)
{
    this->Form::operator=(assignObj);
    return(*this);
}

/* Methods */
void RobotomyRequestForm::execute(Bureaucrat const & executor) const
{
    this->Form::execute(executor); // check authorizations first

    srand(time(0));

    std::cout << "\033[35m* DRIIIIIILING NOISE *\033[m" << std::endl;
    if (std::rand() % 2 == 0)
        std::cout 
            << "\033[35m"
            << this->getTarget()
            << " has been robotomized successfully\033[m"
            << std::endl;
    else
        std::cout 
        << "\033[35mFailure to robotomize "
        << this->getTarget()
        << "\033[m"
        << std::endl;
}