#ifndef ROBOTOMYREQUESTFORM_HPP
#define ROBOTOMYREQUESTFORM_HPP

#include "Form.hpp"

#include <iostream>
#include <cstdlib>

class RobotomyRequestForm : public Form
{
    public:
        /* Constructor */
            RobotomyRequestForm(const std::string& target);
        /* Destructor */
            virtual ~RobotomyRequestForm();
        /* Copy constructor */
            RobotomyRequestForm(const RobotomyRequestForm& copyObj);
        /* Overloading operator */
            RobotomyRequestForm& operator = (const RobotomyRequestForm& assignObj);
        /* Methods */
            virtual void execute (Bureaucrat const & executor) const;
        
    private:
        /* Private constructor */
            RobotomyRequestForm();
};

#endif
