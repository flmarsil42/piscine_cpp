#ifndef BUREAUCRAT_HPP
#define BUREAUCRAT_HPP

#include "Form.hpp"

#include <iostream>
#include <string>
#include <exception>

class Form;

class Bureaucrat
{
    public:
        /* Constructor */
            Bureaucrat(std::string const name, int grade);
        /* Destructor */
            ~Bureaucrat();
        /* Copy constructor */
            Bureaucrat(const Bureaucrat& copyObj);
        /* Overloading oprator */
            Bureaucrat& operator = (const Bureaucrat& assignObj);
        /* Methods */
            int getGrade() const;
            const std::string& getName() const;
            void upGrade();
            void downGrade();
            void signForm(Form& form) const;

        /* Classes exceptions */
        class GradeTooHighException : public std::exception
        {
            public:
                    GradeTooHighException() throw();
                    virtual const char* what() const throw();
        };

        class GradeTooLowException : public std::exception
        {
            public:
                    GradeTooLowException() throw();
                    virtual const char* what() const throw();
        };

    private:
        /* Attributs*/
            std::string     _name;
            int             _grade;     // 1 > 150
};

std::ostream& operator << (std::ostream& flux, const Bureaucrat& bureaucrat);

#endif
