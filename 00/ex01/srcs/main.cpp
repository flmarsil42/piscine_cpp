/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/11 12:51:05 by a42               #+#    #+#             */
/*   Updated: 2020/12/21 19:35:24 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/Phonebook.class.hpp"
#include "../includes/Contacts.class.hpp"

int		main(int ac, char **av)
{
	(void)av;
	(void)ac;
	Phonebook 		awesome;

	if (ac > 1)
		std::cout << "Erreur : lancez le programme sans arguments" << std::endl;
	else
		awesome.func_boucle();
	return (0);
}
