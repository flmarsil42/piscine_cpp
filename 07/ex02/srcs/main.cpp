#include "../includes/Array.hpp"

int main()
{
    srand(time(0));
    size_t size = 15;

    std::cout << "\n****** Creation int array with no parameters ******\n" << std::endl;

    Array<int> noParamArray;
    std::cout << noParamArray << std::endl;
    std::cout << "noParamArray address = " << &noParamArray << std::endl;

    std::cout << "\n****** Creation int array with no values ******\n" << std::endl;
    Array<int> intArray(size);
    std::cout << intArray << std::endl;
    std::cout << "intArray address = " << &intArray << std::endl;

    std::cout << "\n****** Copy creation from first int array ******\n" << std::endl;
    Array<int> copyArray(intArray);
    std::cout << copyArray << std::endl;
    std::cout << "CopyArray address = " << &copyArray << std::endl;

    std::cout << "\n****** Values modifications in the first int array ******\n" << std::endl;
    for (size_t i = 0 ; i < size ; i++)
        intArray[i] = rand() % 100;
    std::cout << intArray << std::endl;
    std::cout << "intArray address = " << &intArray << std::endl;

    std::cout << "\n****** Print the copy array a second time to check values has not changed ******\n" << std::endl;

    std::cout << copyArray << std::endl;
    std::cout << "CopyArray address = " << &copyArray << std::endl;

    std::cout << "\n****** Copy creation from first int array after modifications ******\n" << std::endl;
    Array<int> secondCopyArray(intArray);
    std::cout << secondCopyArray << std::endl;
    std::cout << "secondCopyArray address = " << &secondCopyArray << std::endl;

    std::cout << "\n****** Test overloading operator [] and = with float array ******\n" << std::endl;

    std::cout << "Creation of float array with size 5" << std::endl;
    Array<float> floatArray(5);
    std::cout << floatArray << std::endl;
    std::cout << "floatArray address = " << &floatArray << std::endl;

    std::cout << "\nPut in some values" << std::endl;

    floatArray[0] = 45.454;
    floatArray[1] = 4.5;
    floatArray[2] = 334.4;
    floatArray[3] = 111.9;
    std::cout << floatArray << std::endl;
    std::cout << "floatArray address = " << &floatArray << std::endl;

    std::cout << "\nTry to access to values 3 and 1 with [] operator\n" << std::endl;

    std::cout << "floatArray[3] = " << floatArray[3] << std::endl;
    std::cout << "floatArray[1] = " << floatArray[1] << std::endl;

    std::cout << "\nCreation of second float array and use = operator \n" << std::endl;
    Array<float> secondFloatArray = floatArray;
    std::cout << secondFloatArray << std::endl;
    std::cout << "secondFloatArray address = " << &secondFloatArray << std::endl;

    std::cout << "\nTry to access to values over the size array, for example 6\n" << std::endl;

    try
    {
        std::cout << "secondfloatArray[6] = " << secondFloatArray[6] << std::endl;
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    // system("leaks a.out");
}
