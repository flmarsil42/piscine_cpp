#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <iostream>
#include <exception>
#include <cstdlib>

template <typename T>
class Array
{
    public:
        /* Constructor */
            Array();
            Array(size_t size);
        /* Destructor */
            ~Array();
        /* Copy constructor */
            Array(const Array<T>& copyObj);
        /* Methods */
            size_t getSize() const;
        /* Overloading operator */
            T& operator [] (const size_t& pos);
            const T& operator [] (const size_t& pos) const;

            T& operator = (const Array<T>& assignObj) const;

    private:
        /* Attributs */
            T*      _rawArray;
            size_t  _size;
};

/* Overloading operator */
template <typename T>
std::ostream& operator << (const std::ostream& output, const Array<T>& array);

/* Overloading operator implementation */
template <typename T>
std::ostream& operator << (std::ostream& output, const Array<T>& array)
{
    output
        << "[";
    for (size_t i = 0 ; i < array.getSize() ; i++)
    {
        output << array[i];
        (i < array.getSize() - 1) ? output << ", " : 0;
    }
    output << "]" << std::endl;
    return (output);
}

template <typename T>
const T& Array<T>::operator [] (const size_t& pos) const
{
        if (pos >= this->_size)
            throw std::exception();
    return (this->_rawArray[pos]);
}

template <typename T>
T& Array<T>::operator [] (const size_t& pos)
{
        if (pos >= this->_size)
            throw std::exception();
    return (this->_rawArray[pos]);
}

template <typename T>
T& Array<T>::operator = (const Array<T>& assignObj) const
{
    this->_size = assignObj.getSize();
    this->_rawArray = new T [this->_size];
    for (size_t i = 0 ; i < this->_size ; i++)
        this->_rawArray[i] = assignObj._rawArray[i];
}

/* Constructor implementation */
template <typename T>
Array<T>::Array() : _rawArray(new T[0]), _size(0) {}

template <typename T>
Array<T>::Array(size_t size) : _rawArray(new T[size]), _size(size)
{
    // init all to 0; -> example : float i = float(); -> i = 0;
    for (size_t i = 0 ; i < this->_size ; i++)
        this->_rawArray[i] = T();
}

/* Destructor implementation */
template <typename T>
Array<T>::~Array() 
{
    delete [] this->_rawArray;
}

/* Copy constructor implementation */
template <typename T>
Array<T>::Array(const Array<T>& copyObj)
{
    this->_size = copyObj.getSize();
    this->_rawArray = new T[this->_size];
    for (size_t i = 0 ; i < this->_size ; i++)
        this->_rawArray[i] = copyObj._rawArray[i];
}

/* Methods implementation */
template <typename T>
size_t Array<T>::getSize() const
{
    return (this->_size);
}

#endif
