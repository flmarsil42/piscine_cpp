#include "../includes/span.hpp"

#define SIZE 1000
#define OUT 0

class FillMultiSet
{
	public:

		FillMultiSet(int value = 0) : _value(value) {};
		~FillMultiSet() {};

		int operator()() {return (_value++);};

	private:

		int	_value;	
};


int main ()
{
    srand(time(0));
    Span S(SIZE);

    try
    {
        for (int i = 0 ; i < SIZE + OUT ; i++)
            S.addNumber(i + (rand() % 10));
        
        std::cout << S.shortestSpan() << std::endl;
        std::cout << S.longestSpan() << std::endl;
        
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
    
    std::cout << "\n--------- Array of 10000 ints ---------\n";
    Span span(10000);
    FillMultiSet func(0);

	std::generate_n(std::inserter(span.getVector(), span.getVector().begin()), span.getStorage(), func);

    for (std::vector<int>::iterator it = span.getVector().begin() ; it != span.getVector().begin() + 10 ; ++it)
        std::cout << *it << std::endl;
    
    std::cout << "\n--------- Span with 10000 ---------\n";
    std::cout << span.shortestSpan() << std::endl;
    std::cout << span.longestSpan() << std::endl;
    return (0);
}


        
// std::cout << "----------\n";
// std::vector<int>::iterator it;
// for (it = S.begin() ; it != S.end() ; ++it)
//     std::cout << *it << std::endl;
// std::cout << "----------\n";