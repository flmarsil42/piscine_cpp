#include "../includes/easyfind.hpp"


#include <vector>

// int main ()
// {
//     std::vector<int> G;
//     std::vector<int>::iterator it;

//     for (int i = 0 ; i < 10 ; i++)
//         G.push_back(i + 3);
    
//     std::cout << "***** VALID TEST*****" << std::endl;
//     try
//     {
//         it = easyfind(G, 6);
//         std::cout << *it << std::endl;
//     }
//     catch (std::exception& e)
//     {
//         std::cerr << e.what() << std::endl;
//     }

//     std::cout << "***** ERREUR TEST *****" << std::endl;
//     try
//     {
//         it = easyfind(G, 555);
//         std::cout << *it << std::endl;
//     }
//     catch (std::exception& e)
//     {
//         std::cerr << e.what() << std::endl;
//     }

//    return (0);
// }


int main ()
{
    std::vector<int> G;
    std::vector<int>::const_iterator it;

    for (int i = 0 ; i < 10 ; i++)
        G.push_back(i + 3);
    
    std::cout << "***** VALID TEST*****" << std::endl;
    try
    {
        it = easyfind(G, 6);
        std::cout << *it << std::endl;
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

    std::cout << "***** ERREUR TEST *****" << std::endl;
    try
    {
        it = easyfind(G, 555);
        std::cout << *it << std::endl;
    }
    catch (std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }

   return (0);
}
