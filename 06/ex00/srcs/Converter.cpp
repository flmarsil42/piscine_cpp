# include <iostream>    // std::
# include <sstream>     // string stream
# include <cmath>       // isnan
# include <iomanip>     // set_precision
# include <limits.h>    // INT_MAX INT_MIN

#define PRECISION 1

void    toChar(double value, bool good)
{
    std::cout << "char: ";

    if (good == true && std::isnan(value) == 0)
    {
        char c = static_cast<char>(value);
        if (std::isprint(c))
            std::cout
                <<"'"
                << c
                <<"'"
                << std::endl;
        else
            std::cout
                << "Non displayable"
                << std::endl;
    }
    else
        std::cout
            << "impossible"
            << std::endl;
}

void    toInt(double value, bool good)
{
    std::cout << "int: ";

    if (good == true && (value <= INT_MAX && value >= INT_MIN))
        std::cout
            << static_cast<int>(value)
            << std::endl;
    else
        std::cout
            << "impossible"
            << std::endl;
}

void    toFloat(double value, bool good)
{
    std::cout << "float: ";

    if (good == true)
        std::cout
            << std::fixed
            << std::setprecision(PRECISION)
            << static_cast<float>(value)
            << "f"
            << std::endl;
    else
        std::cout
            << "impossible"
            << std::endl;
}

void    toDouble(double value, bool good)
{
    std::cout << "double: ";

    if (good == true)
        std::cout
            << std::fixed
            << std::setprecision(PRECISION)
            << static_cast<double>(value)
            << std::endl;
    else
        std::cout
            << "impossible"
            << std::endl;
}

int     main(int ac, char **av)
{
    if (ac != 2) { std::cout << "Error : Use : ./a.out 'input'\n" ; return (1) ;}

    bool    good = true;
    double  value;

    std::string s = av[1];

    if (s.size() == 1 && std::isprint(s[0]) && !std::isdigit(s[0]))
    {   
        char c;
        std::stringstream ss;
        ss << s;
        ss >> c;
        value = static_cast<int>(c);
    }
    else
    {
        try { value = atof(av[1]); }
        catch (std::exception& e) { good = false; }
    }

    toChar(value, good);
    toInt(value, good);
    toFloat(value, good);
    toDouble(value, good);
    return (0);
}
