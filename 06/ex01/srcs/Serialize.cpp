# include <iostream>
# include <cstdlib>
# include <limits.h>

# define MAX INT_MAX
# define LEN_LINE 8

typedef struct      s_data
{
    std::string     str1;
    int             number;
    std::string     str2;
}                   t_data;

void alpha_num_generator(char* p_data)
{
    const std::string random = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for (int i = 0 ; i < LEN_LINE ; i ++)
    {
        *p_data = random[rand() % random.size()];
        std::cout << *p_data++;
    }
    std::cout << std::endl;
}

void num_generator(char* p_data)
{
	*(reinterpret_cast<int*>(p_data)) = rand() % MAX;
	std::cout << *(reinterpret_cast<int*>(p_data)) << std::endl;
}

void* serialize()
{
    void *p_data = new char[20];

    std::cout << "Random string 1 : ";
    alpha_num_generator(reinterpret_cast<char*>(p_data));

    std::cout << "Random int      : ";
    num_generator(reinterpret_cast<char*>(p_data) + LEN_LINE);

    std::cout << "Random string 2 : ";
    alpha_num_generator(reinterpret_cast<char*>(p_data) + LEN_LINE + 4);

    return (p_data);
}

t_data* deserialize(void* p_data)
{
    char* heap_line = reinterpret_cast<char*>(p_data);

    t_data* Data = new t_data;

    Data->str1.resize(9);
    for (int i = 0 ; i < LEN_LINE ; i++)
        Data->str1[i] = *heap_line++;
    Data->str1[LEN_LINE] = '\0';

    Data->number = *(reinterpret_cast<int*>(heap_line));
    heap_line += 4;

    Data->str2.resize(9);
    for (int i = 0 ; i < LEN_LINE ; i++)
        Data->str2[i] = *heap_line++;
    Data->str2[LEN_LINE] = '\0';
    return (Data);
}

int     main()
{
    std::srand(time(0));

    std::cout << std::endl;
    std::cout << "-------- Serialize called --------" << std::endl;

    void* p_data = serialize();

    std::cout << std::endl;
    std::cout << "-------- Deserialize called --------" << std::endl;

    t_data *Data = deserialize(p_data);

    std::cout
        << Data->str1 << std::endl
        << Data->number << std::endl
        << Data->str2 << std::endl;
    std::cout << std::endl;

    delete (char*) p_data;
    delete Data;

    // system("leaks a.out");
    return (0);
}
