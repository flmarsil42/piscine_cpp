#include "../includes/ClapTrap.hpp"

//---------------------------------------------------
// 			CONSTRUCTEURS/DECONSTRUCTEURS 			|
//---------------------------------------------------

// constructeur - initialisation des variables
ClapTrap::ClapTrap(const std::string& name) : _hit_point(100), _max_hit_point(100),
_energy_point(100), _max_energy_point(100), _level(1), _melee_attack_dmg(30),
_ranged_attack_dmg(20), _armor_dmg_reduction(2), _name(name), _type("*CL4P-TP")
{
    std::cout << "\033[36mClapTrap - Constructor is called\033[m" << std::endl;
    srand(time(0));
}

// destructeur
ClapTrap::~ClapTrap(void){
    std::cout << "\033[36mClapTrap - Destructor is called\033[m" << std::endl;
}

//---------------------------------------------------
// 						METHODES					|
//---------------------------------------------------

// take damage
void
ClapTrap::takeDamage(unsigned int amount)
{
    unsigned int max = this->_max_energy_point;
    unsigned int& life = this->_energy_point;
    unsigned int armor = this->_armor_dmg_reduction;
    unsigned int taken = (amount <= armor) ? 0 : amount - armor;        // reduction des dommages 
    unsigned int temp = taken;                                          // permet l'affichage des dommages réels si taken > life

    (life <= taken) ? taken = life : 0;                                 // protection pour ne pas passer à -X pv
    std::cout << "\033[31m" << this->_type << " " << this->_name << " subit " 
    << temp << " points de dégâts ! (pdv = " << (life -= taken) << "/" << max << ")\033[m" << std::endl;
    return ;
}

// be repaired
void
ClapTrap::beRepaired(unsigned int amount){
    unsigned int max = this->_max_energy_point;
    unsigned int& life = this->_energy_point;

    life += amount;                                                 // calcul vie + montant heal
    (life > max) ? life = max : 0;                                  // protection contre heal > max life
    std::cout << "\033[32m" << this->_type << " " << this->_name << " se soigne de " 
    << amount << " points de vie ! (pdv = " << life << "/" << max << ")\033[m" << std::endl;
    return ;
}

// range attack
void
ClapTrap::rangedAttack(std::string const& target){
    std::cout << "\033[33m" << this->_type << " " << this->_name << " attaque " 
        << target << " à distance, causant " << this->_ranged_attack_dmg 
        << " points de dégâts à sa cible!\033[m" << std::endl;
    return;
}

// melee attack
void
ClapTrap::meleeAttack(std::string const& target){
    std::cout << "\033[33m" << this->_type << " " << this->_name << " attaque " 
    << target << " au corps-à-corps , causant " << this->_melee_attack_dmg
    << " points de dégâts à sa cible!\033[m" << std::endl;

    return;
}

void ClapTrap::printattr() const
{
	std::cout << "name: " << this->_name << std::endl;
	std::cout << "hp: "	<< this->_hit_point << std::endl;
	std::cout << "max hp: " << this->_max_hit_point << std::endl;
	std::cout << "energy: " << this->_energy_point << std::endl;
	std::cout << "max energy: " << this->_max_energy_point << std::endl;
	std::cout << "melee atta: " << this->_melee_attack_dmg << std::endl;
	std::cout << "ranged att: " << this->_ranged_attack_dmg << std::endl;
	std::cout << "armor reduc: " << this->_armor_dmg_reduction<< std::endl;
}