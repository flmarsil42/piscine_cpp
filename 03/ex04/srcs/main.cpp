#include <cstdlib>
#include "../includes/FragTrap.hpp"
#include "../includes/ScavTrap.hpp"
#include "../includes/NinjaTrap.hpp"
#include "../includes/SuperTrap.hpp"

int main()
{
	// For generating random numbers in vaulthunterDotExe and challengeNewcomer
	srand(time(0));

	NinjaTrap idiot("idiot");
	idiot.printattr();
	idiot.takeDamage(100);

	FragTrap idiot2("idiot2");
	idiot2.printattr();
	idiot2.takeDamage(50);

	// Building SuperTrap using virtual classes (diamond problem)
	// >> each constructor is only called once : ClapTrap, FragTrap, NinjaTrap, SuperTrap
	SuperTrap retard("retard");

	std::cout << "\n\n";
	retard.printattr();
	std::cout << "\n\n";
	
	// // Demonstrating Supertrap inherited methods from both classes
	retard.rangedAttack("Handsome Jack");                   // FragTrap ranged attack
	retard.meleeAttack("Handsome Jack");                    // NinjaTrap melee attack
	retard.ninjaShoebox(idiot);                             // NinjaTrap special attack
	retard.vaulthunter_dot_exe("Handsome Jack");              // FragTrap special attack
	std::cout << std::endl;

	return (0);
}