#include "../includes/SuperTrap.hpp"

SuperTrap::SuperTrap(std::string name) : ClapTrap(name), NinjaTrap(""), FragTrap(""){
    
    std::cout << "SuperTrap - Constructor called" << std::endl;
	this->_name = name;
	this->_level = 1;
	this->_type = "Super-TP";
	this->_hit_point = 100;
    this->_max_hit_point = 100;
	this->_energy_point = 120;
    this->_max_energy_point = 120;
	this->_melee_attack_dmg = 60;
	this->_ranged_attack_dmg = 20;
    this->_armor_dmg_reduction = 5;
}

SuperTrap::~SuperTrap(){
	std::cout << "Super Trap - Destructor called" << std::endl;
}

void
SuperTrap::rangedAttack( const std::string & target)
{
	FragTrap::rangedAttack(target);
}

void
SuperTrap::meleeAttack(const std::string & target)
{
	NinjaTrap::meleeAttack(target);
}

void SuperTrap::printattr() const
{
	std::cout << "\nATTRIBUTS SUPER-TRAP\n" << std::endl;
	ClapTrap::printattr();
}
