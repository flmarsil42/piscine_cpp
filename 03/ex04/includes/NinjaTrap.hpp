#ifndef NINJATRAP_HPP
#define NINJATRAP_HPP

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap : public virtual ClapTrap {

    public:
	    // constructeurs & destructeurs
        NinjaTrap(std::string name);
        ~NinjaTrap();

        // méthodes
        void ninjaShoebox(ClapTrap& clap);
    	void ninjaShoebox(FragTrap& frag);
        void ninjaShoebox(ScavTrap& scav);
        void ninjaShoebox(NinjaTrap& ninja);
        void printattr() const;

    private:
	    NinjaTrap();
};


#endif
