#include "../includes/ScavTrap.hpp"

//---------------------------------------------------
// 			CONSTRUCTEURS/DECONSTRUCTEURS 			|
//---------------------------------------------------

// constructeur - initialisation des variables
ScavTrap::ScavTrap(std::string name) : ClapTrap(name)
{
    this->_hit_point = 100;
    this->_max_hit_point = 100;
    this->_enegery_point = 50;
    this->_max_energy_point = 50;
    this->_level = 1;
    this->_melee_attack_dmg = 20;
    this->_ranged_attack_dmg = 15;
    this->_armor_dmg_reduction = 3;
    this->_type = "*SC4V-TP";
    std::cout << "\033[36mScavTrap - Constructor is called\033[m" << std::endl;
    srand(time(0));
}

// destructeur
ScavTrap::~ScavTrap(void){
    std::cout << "\033[36mScavTrap - Destructor is called\033[m" << std::endl;
}

//---------------------------------------------------
// 						METHODES					|
//---------------------------------------------------

static std::string r_challenge[5] = { "Challenge 500 push up", 
                            "Challenge 300 deep squat",
                            "Challenge 100 tractions",
                            "Challenge 500 dips",
                            "Challenge 4000 abdos" };

void
ScavTrap::challengeNewcomer(std::string const& target){
    unsigned int& hit_p = this->_hit_point;
    unsigned int max_hit_p = this->_max_hit_point;
    unsigned int cost = 25;
    unsigned int random = rand() % 5;

    if (hit_p < cost)
        std::cout << "\033[36mPas assez d'énergie pour lancer un autre challenge random\n\033[m";
    else
        {
            std::cout << "\033[35m" << this->_type << " " << this->_name << " lance " 
            << r_challenge[random] << " sur " << target
            << " et dépense " << cost << " d'énergie."
            << "(énergie = " << (hit_p -= cost) << "/" << max_hit_p << ")" << std::endl;
        }
    return ;
}
