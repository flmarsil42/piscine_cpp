#include "../includes/NinjaTrap.hpp"

//---------------------------------------------------
// 			CONSTRUCTEURS/DECONSTRUCTEURS 			|
//---------------------------------------------------

// constructeur - initialisation des variables
NinjaTrap::NinjaTrap(const std::string&  name) : ClapTrap(name)
{
    this->_hit_point = 60;
    this->_max_hit_point = 60;
    this->_enegery_point = 120;
    this->_max_energy_point = 120;
    this->_level = 1;
    this->_melee_attack_dmg = 60;
    this->_ranged_attack_dmg = 5;
    this->_armor_dmg_reduction = 0;
    this->_type = "*NJ4-TP";

    std::cout << "\033[36mNJ4-TP - Constructor is called\033[m" << std::endl;
    srand(time(0));
}

// destructeur
NinjaTrap::~NinjaTrap(void){
    std::cout << "\033[36mNJ4-TP - Destructor is called\033[m" << std::endl;
}

//---------------------------------------------------
// 						METHODES					|
//---------------------------------------------------

void NinjaTrap::ninjaShoebox(ClapTrap& clap){
    std::cout << this->_type << " lance des fumigènes\n";
    clap.takeDamage(50);
    return;
}

void NinjaTrap::ninjaShoebox(FragTrap& frag){
    std::cout << this->_type << " lance des shurikens\n";
    frag.takeDamage(90);
    return;
}

void NinjaTrap::ninjaShoebox(ScavTrap& scav){
    std::cout << this->_type << " soigne son ennemi sans faire exprès\n";
    scav.beRepaired(45);
    return;
}

void NinjaTrap::ninjaShoebox(NinjaTrap& ninja){
    std::cout << this->_type << " essaye de prendre la fuite face à lui même et tombe puit meurt\n";
    ninja.takeDamage(120);
    return;
}