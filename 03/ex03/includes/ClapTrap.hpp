#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

#include <iostream>
#include <time.h>
#include <stdlib.h>

class ClapTrap {

    public:
        // constructeur / destructeur
        ClapTrap(std::string name);
        ~ClapTrap();

        // méthodes
        void rangedAttack(std::string const & target);
        void meleeAttack(std::string const & target);
        void takeDamage(unsigned int amount);
        void beRepaired(unsigned int amount);

    protected:
        // attributs
        unsigned int _hit_point;
        unsigned int _max_hit_point;
        unsigned int _enegery_point;
        unsigned int _max_energy_point;
        unsigned int _level;
        unsigned int _melee_attack_dmg;
        unsigned int _ranged_attack_dmg;
        unsigned int _armor_dmg_reduction;
        std::string _name;
        std::string _type;
};

#endif