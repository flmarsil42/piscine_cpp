#include "../includes/FragTrap.hpp"

//---------------------------------------------------
// 			CONSTRUCTEURS/DECONSTRUCTEURS 			|
//---------------------------------------------------

// constructeur - initialisation des variables
FragTrap::FragTrap(std::string name) : _hit_point(100), _max_hit_point(100),
_enegery_point(100), _max_energy_point(100), _level(1), _melee_attack_dmg(30),
_ranged_attack_dmg(20), _armor_dmg_reduction(5), _name(name)
{
    std::cout << "\033[36mConstructor is called\033[m" << std::endl;
    (void)this->_level;
    srand(time(NULL));
}

// destructeur
FragTrap::~FragTrap(void){
    std::cout << "\033[36mDestructor is called\033[m" << std::endl;
}

//---------------------------------------------------
// 						METHODES					|
//---------------------------------------------------

// take damage
void
FragTrap::takeDamage(unsigned int amount)
{
    unsigned int& life = this->_enegery_point;
    unsigned int armor = this->_armor_dmg_reduction;
    unsigned int taken = (amount <= armor) ? 0 : amount - armor;        // reduction des dommages 
    unsigned int temp = taken;                                          // permet l'affichage des dommages réels si taken > life

    (life <= taken) ? taken = life : 0;                                 // protection pour ne pas passer à -X pv
    std::cout << "\033[31mFR4G-TP " << this->_name << " subit " 
    << temp << " points de dégâts ! (pdv = " << (life -= taken) << "/100)\033[m" << std::endl;
    return ;
}

// be repaired
void
FragTrap::beRepaired(unsigned int amount){
    unsigned int& life = this->_enegery_point;
    unsigned int max = this->_max_energy_point;

    life += amount;                                                 // calcul vie + montant heal
    (life > max) ? life = max : 0;                                  // protection contre heal > max life
    std::cout << "\033[32mFR4G-TP " << this->_name << " se soigne de " 
    << amount << " points de vie ! (pdv = " << life << "/100)\033[m" << std::endl;
    return ;
}

// range attack
void
FragTrap::rangedAttack(std::string const& target){
    std::cout << "\033[33mFR4G-TP " << this->_name << " attaque " 
        << target << " à distance, causant " << this->_ranged_attack_dmg 
        << " points de dégâts à sa cible!\033[m" << std::endl;
    return;
}

// melee attack
void
FragTrap::meleeAttack(std::string const& target){
    std::cout << "\033[33mFR4G-TP " << this->_name << " attaque " 
    << target << " au corps-à-corps , causant " << this->_melee_attack_dmg
    << " points de dégâts à sa cible!\033[m" << std::endl;

    return;
}

static std::string r_attack[5] = { "Attaque trempette", 
                            "Prendre la fuite",
                            "Attaque éclair",
                            "Attaque contrôle mental",
                            "Attaque flamiche" };
void
FragTrap::vaulthunter_dot_exe(std::string const& target){
    unsigned int& hit_p = this->_hit_point;
    unsigned int max_hit_p = this->_max_hit_point;
    unsigned int cost = 25;
    unsigned int random = (rand() % 5);

    if (hit_p < cost)
        std::cout << "\033[36mPas assez d'énergie pour lancer une autre attaque random\n\033[m";
    else
        {
            std::cout << "\033[35mFR4G-TP " << this->_name << " lance " 
            << r_attack[random] << " sur " << target
            << " et dépense " << cost << " d'énergie."
            << "(énergie = " << (hit_p -= cost) << "/" << max_hit_p << ")" << std::endl;
        }
    return ;
}


