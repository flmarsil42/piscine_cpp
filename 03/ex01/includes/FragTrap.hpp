#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include <iostream>
#include <time.h>
#include <stdlib.h>

class FragTrap {

    public:

        // constructeur / destructeur
        FragTrap(std::string name);
        ~FragTrap(void);


        // méthodes
        void rangedAttack(std::string const & target);
        void meleeAttack(std::string const & target);
        void takeDamage(unsigned int amount);
        void beRepaired(unsigned int amount);

        void vaulthunter_dot_exe(std::string const & target);

    private:

        // attributs
        unsigned int _hit_point;
        unsigned int _max_hit_point;
        unsigned int _enegery_point;
        unsigned int _max_energy_point;
        unsigned int _level;
        unsigned int _melee_attack_dmg;
        unsigned int _ranged_attack_dmg;
        unsigned int _armor_dmg_reduction;
        std::string _name;

};

#endif