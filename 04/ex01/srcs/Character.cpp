#include "../includes/Character.hpp"
#include "../includes/Enemy.hpp"

/* Constructor */
Character::Character(std::string const & name)
    : _name(name), _actionPoint(40), _weapon(0) {}

/* Destructor */
Character::~Character() {}

/* Copy constructor */
Character::Character(const Character& copyObj) 
    : _name(copyObj._name), _actionPoint(copyObj._actionPoint) {}

/* Operation overload */
Character& Character::operator = (const Character& copyObj)
{
    if (this == &copyObj)
        return (*this);
    this->_name = copyObj.getName();
    this->_actionPoint = copyObj.getApoint();
    this->_weapon = copyObj.getWeapon();
    return (*this);
}

/* Method */
std::string
Character::getName() const
{
    return (this->_name);
}

AWeapon*
Character::getWeapon() const
{
    return (this->_weapon);
}

unsigned int
Character::getApoint() const
{
    return (this->_actionPoint);
}

void
Character::recoverAP()
{
    int& Ap = this->_actionPoint;

    Ap += 10;
    (Ap > 40) ? Ap = 40 : 0;
    std::cout
        << this->getName()
        << " has recover AP! He got now " 
        << this->getApoint()
        << "AP."
        << std::endl;
}

void
Character::equip(AWeapon* aweapond)
{
    this->_weapon = aweapond;
}

/*  Attaque l'ennemi si le personnage à une arme et assez de point d'action */
void
Character::attack(Enemy* enemy)
{
    if (!this->_weapon)
    {
        std::cout
            << this->getName()
            << " has recover AP! He got now " 
            << this->getApoint()
            << "AP."
            << std::endl;
        return ;
    }
    if (this->_actionPoint < this->_weapon->getAPCost())
    {
        std::cout
            << "No Action Point, use recoverAP :("
            << std::endl;
        return ;
    }
    /* Attaque */
    std::cout
        << this->getName()
        << " attaque "
        << enemy->getType()
        << " with a "
        << this->_weapon->getName()
        << std::endl;
    
    this->_actionPoint -= this->_weapon->getAPCost();
    this->_weapon->attack();

    if (enemy->getHP() < this->_weapon->getDamage())
        delete enemy;
    else
        enemy->takeDamage(this->_weapon->getDamage());
}

/* Operation overload << */
std::ostream& operator <<(std::ostream& output, Character& character)
{
	std::cout
        << character.getName()
        << " has " 
        << character.getApoint() 
        << " AP and ";
	if (!character.getWeapon())
		std::cout << "is unarmed\n";
	else
		std::cout 
            <<  "carries a "
            << character.getWeapon()->getName() 
            << "\n";
	return (output);
}