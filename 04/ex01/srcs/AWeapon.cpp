#include "../includes/AWeapon.hpp"

/* Constructor */
AWeapon::AWeapon(std::string const & name, int apcost, int damage)
    : _name(name), _apcost(apcost), _damage(damage) {}

/* Destructor */
AWeapon::~AWeapon() {}

/* Copy constructor */
AWeapon::AWeapon(const AWeapon& copyObj) 
    : _name(copyObj._name), _apcost(copyObj._apcost), _damage(copyObj._damage) {}

/* Operation overload */
AWeapon& AWeapon::operator = (const AWeapon& copyObj)
{
    if (this == &copyObj)
        return (*this);
    this->_name = copyObj.getName();
    this->_apcost = copyObj.getAPCost();
    this->_damage = copyObj.getDamage();
    return (*this);
}

/* Method */
std::string AWeapon::getName() const
{
    return (this->_name);
}

int AWeapon::getAPCost() const
{
    return (this->_apcost);
}

int AWeapon::getDamage() const 
{
    return (this->_damage);
}