#include "../includes/Knife.hpp"

 /* Constructor */
Knife::Knife(std::string const & name, int apcost, int damage)
    : AWeapon(name, apcost, damage) {}

/* Destructor */
Knife::~Knife() {}

/* Copy constructor */
Knife::Knife(const Knife& copyObj)
    : AWeapon(copyObj) {}

/* Operation overload */
Knife& Knife::operator = (const Knife& copyObj)
{
    if (this == &copyObj)
        return (*this);
    this->_name = copyObj.getName();
    this->_apcost = copyObj.getAPCost();
    this->_damage = copyObj.getDamage();
    return (*this);
}

/* Methods */
void Knife::attack() const
{
	std::cout <<  "* knif knif knif *\n";
}

