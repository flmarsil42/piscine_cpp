#include "../includes/PlasmaRifle.hpp"

 /* Constructor */
PlasmaRifle::PlasmaRifle(std::string const & name, int apcost, int damage)
    : AWeapon(name, apcost, damage) {}

/* Destructor */
PlasmaRifle::~PlasmaRifle() {}

/* Copy constructor */
PlasmaRifle::PlasmaRifle(const PlasmaRifle& copyObj)
    : AWeapon(copyObj) {}

/* Operation overload */
PlasmaRifle& PlasmaRifle::operator = (const PlasmaRifle& copyObj)
{
    if (this == &copyObj)
        return (*this);
    this->_name = copyObj.getName();
    this->_apcost = copyObj.getAPCost();
    this->_damage = copyObj.getDamage();
    return (*this);
}

/* Methods */
void PlasmaRifle::attack() const
{
	std::cout <<  "* piouuu piouuu piouuu *\n";
}

