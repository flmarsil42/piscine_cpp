#include "../includes/Enemy.hpp"

/* Constructor  */
Enemy::Enemy(int hp, std::string const & type)
    : _hp(hp), _type(type) {}

/* Destructor */
Enemy::~Enemy() {}

/* Copy constructor */
Enemy::Enemy(const Enemy& copyObj) 
    : _hp(copyObj._hp), _type(copyObj._type) {}

/* Operation overload */
Enemy& Enemy::operator = (const Enemy& copyObj)
{
    if (this == &copyObj)
        return (*this);
    this->_hp = copyObj.getHP();
    this->_type = copyObj.getType();
    return (*this);
}

/* Methods */
const std::string& Enemy::getType() const
{
    return (this->_type);
}

int
Enemy::getHP() const
{
    return (this->_hp);
}

void 
Enemy::takeDamage(int amount)
{
	if (amount < 0)
		return ;
	(amount > this->_hp) ? amount = this->_hp : 0;
	this->_hp -= amount;
}
