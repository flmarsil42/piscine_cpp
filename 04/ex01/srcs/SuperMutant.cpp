#include "../includes/SuperMutant.hpp"

/* Construcor */
SuperMutant::SuperMutant() 
    : Enemy(170, "Super Mutan") 
{
    std::cout
        << "Gaaah. Break everything !"
        << std::endl;
}

/* Destructor */
SuperMutant::~SuperMutant()
{
    std::cout
        << "Aaargh ..."
        << std::endl;
}

/* Copy constructor */
SuperMutant::SuperMutant(const SuperMutant& copyObj)
    : Enemy(copyObj) {}

/* Operation overload  */

SuperMutant& SuperMutant::operator = (const SuperMutant& copyObj)
{
    if (this == &copyObj)
        return (*this);
    
    return (*this);
}

void
SuperMutant::takeDamage(int amount)
{
    Enemy::takeDamage(amount - 3); // overloading resistance 
}