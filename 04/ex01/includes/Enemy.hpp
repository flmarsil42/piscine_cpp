#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <iostream>
#include <string>

class Enemy 
{
    public:
        /* Constructor  */
            Enemy(int hp = 100, std::string const & type = "guardian");
        /* Destructor */
            virtual ~Enemy();
        /* Copy constructor */
            Enemy(const Enemy& copyObj);
        /* Operation overload = */
            Enemy& operator = (const Enemy& copyObj);
        /* Methods */
            const std::string& getType() const;
            int getHP() const;
            virtual void takeDamage(int dmg);

    protected:
        /* Attributs */
            int _hp;
            std::string _type;
};

#endif
