#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include "AWeapon.hpp"
#include "Enemy.hpp"

#include <iostream>

class Character 
{
    public:
        /* Constructor */
            Character(std::string const & name = "Hero");
        /* Destructor */
           virtual ~Character();
        /* Copy constructor */
            Character(const Character& copyObj);
        /* Operation overload */
            Character& operator = (const Character& other);
        /* Methods */
            AWeapon* getWeapon() const;
            unsigned int getApoint() const;

            std::string getName() const;
            void recoverAP();
            void equip(AWeapon* aweapond);
            void attack(Enemy* enemy);

    protected:
        /* Attributs */
            std::string     _name;
            int             _actionPoint;
            AWeapon*        _weapon;
};

std::ostream& operator<<(std::ostream& output, Character& character);

#endif
