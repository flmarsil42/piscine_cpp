#ifndef AWEAPON_HPP
#define AWEAPON_HPP

#include <iostream>

class AWeapon
{
    public:
        /* Constructor */
            AWeapon(std::string const & name, int apcost, int damage);
        /* Destructor */
           virtual ~AWeapon();
        /* Copy constructor */
            AWeapon(const AWeapon& copyObj);
        /* Operation overload */
            AWeapon& operator = (const AWeapon& other);
        /* Methods */
            std::string getName() const;
            int getAPCost() const;
            int getDamage() const;
            virtual void attack() const = 0;

    protected:
        /* Attributs */
            std::string _name;
            int _apcost;
            int _damage;

};

#endif
