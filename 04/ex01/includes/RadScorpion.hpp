#ifndef RADSCORPION_HPP
# define RADSCORPION_HPP

#include "../includes/Enemy.hpp"

class RadScorpion : public Enemy
{   
    public:
        /* Construcor */
            RadScorpion();
        /* Destructor */
            virtual ~RadScorpion();
        /* Copy constructor */
            RadScorpion(const RadScorpion& copyObj);
        /* Operation overload  */
            RadScorpion& operator = (const RadScorpion& copyObj);
        /* Methods */
            virtual void takeDamage(int amount);
};

#endif
