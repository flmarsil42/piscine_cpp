#ifndef POWERFIST_HPP
#define POWERFIST_HPP

#include "AWeapon.hpp"

class PowerFist : public AWeapon
{
    public:
        /* Constructor */
            PowerFist(std::string const & name = "Power Fist", int apcost = 8, int damage = 50);
        /* Destructor */
           virtual ~PowerFist();
        /* Copy constructor */
            PowerFist(const PowerFist& copyObj);
        /* Operation overload */
            PowerFist& operator = (const PowerFist& other);
        /* Methods */
            virtual void attack() const;
};

#endif
