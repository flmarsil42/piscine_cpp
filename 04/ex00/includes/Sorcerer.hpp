#ifndef SORCERER_HPP
#define SORCERER_HPP

#include <iostream>
#include "../includes/Victim.hpp"

class Sorcerer {
    
    public:

        /* Constructor  */
        Sorcerer(const std::string& name = "Gandalf", const std::string& title = "De base");

        /* Destructor */
        ~Sorcerer();

        /* Copy constructor */
        Sorcerer(const Sorcerer&);

        /* Operation overload << */
        Sorcerer& operator = (const Sorcerer& copyObj);

        /* Methods */
        const std::string& getName() const;
        const std::string& getTitle() const;
        void polymorph(Victim const & victim);

    private:

        Sorcerer();
        /* Attributs */
        std::string _name;
        std::string _title;

};

/* Operation overload << */
std::ostream& operator << (std::ostream& flux, const Sorcerer& sorcerer);

#endif
