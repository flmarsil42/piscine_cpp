#ifndef VICTIM_HPP
#define VICTIM_HPP

#include <iostream>

class Victim {

    public:

        /* Constructor  */
        Victim(const std::string& name);

        /* Destructor */
        virtual ~Victim();
    
        /* Copy constructor */
        Victim(const Victim&);

        /* Operation overload = */
        Victim& operator = (const Victim& copyObj);

        /* Methods */
        const std::string& getName() const;
        virtual void getPolymorphed() const;

    private:

        Victim();

        /* Attributs */
            std::string _name;
};

/* Operation overload << */
std::ostream& operator << (std::ostream& flux, const Victim& victim);

#endif
