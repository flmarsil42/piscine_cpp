#include "../includes/Sorcerer.hpp"


/* Constructor  */
Sorcerer::Sorcerer(const std::string& name, const std::string& title)
: _name(name), _title(title)
{
    if (_name != "" && _title != "")
        std::cout
            << "\033[36m"
            << this->_name
            << ", "
            << this->_title
            << ", is born!\033[m"
            << std::endl;
    else
    {
        std::cout 
            << "\033[36mName or / and title of Sorcerer forbidden :(\033[m"
            << std::endl;
    }
}

/* Destructor */
Sorcerer::~Sorcerer()
{
    std::cout
        << "\033[31m"
        << this->_name
        << ", "
        << this->_title
        << ", is dead. Consequences will never be the same !\033[m"
        << std::endl;
}

/* Copy constructor */
Sorcerer::Sorcerer(const Sorcerer& copyObj)
    : _name(copyObj._name), _title(copyObj._title) {}


/* Methods */
const std::string& Sorcerer::getName() const
{
	return (this->_name);
}

const std::string& Sorcerer::getTitle() const
{
	return (this->_title);
}

void
Sorcerer::polymorph(Victim const & victim)
{
    victim.getPolymorphed();
    return ;
}

/* Operation overload << */
std::ostream& operator << (std::ostream& flux, const Sorcerer& sorcerer)
{
    flux 
        << "\033[32m"
        << "I am " 
        << sorcerer.getName() 
        << ", " 
        << sorcerer.getTitle() 
        << ", and I like ponies!\n\033[m";
    return (flux);
}

/* Operation overload = */
Sorcerer& Sorcerer::operator = (const Sorcerer& copyObj)
{
    this->_name = copyObj.getName();
    this->_title = copyObj.getTitle();
    return (*this);
}