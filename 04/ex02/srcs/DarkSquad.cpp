#include "../includes/DarkSquad.hpp"

/* Constructor */
DarkSquad::DarkSquad() 
    : _count(0), _unit(0) {}

/* Destructor */
DarkSquad::~DarkSquad()
{
    for (int i = 0 ; i < this->_count ; i++)
        delete this->_unit[i];
    delete[] this->_unit;
}

/* Copy constructor */
DarkSquad::DarkSquad(const DarkSquad& copyObj)
{
    this->_count = copyObj.getCount();
 
    // Create new tab and copy all tab in
    this->_unit = new ISpaceMarine* [this->_count];
    for (int i = this->_count ; i > 0 ; --i)
        this->_unit[i] = copyObj._unit[i]->clone();
}

/* Overloading operator */
DarkSquad& DarkSquad::operator = (const DarkSquad& assignObj) 
{
    this->_count = assignObj.getCount();
    this->_unit = assignObj._unit;
    return (*this);
}

/* Method */

int
DarkSquad::getCount() const
{
    return (this->_count);
}

int
DarkSquad::push(ISpaceMarine* newUnit)
{
    ISpaceMarine** tmp = this->_unit;

    int size = this->getCount() + 1;
    
    // delete this->_unit;
    this->_unit = new ISpaceMarine*[size];

    // copying the previous array + new
    for (int i = 0 ; i < size - 1 ; i++)
        this->_unit[i] = tmp[i];
    this->_unit[size - 1] = newUnit;

    this->_count++;
    delete[] tmp;
    return (this->_count);
}

ISpaceMarine* DarkSquad::getUnit(int n) const
{
    if (n > this->_count || n < 0)
        return (0);
    return (this->_unit[n]);
}