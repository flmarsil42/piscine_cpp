#include "../includes/Squad.hpp"

/* Constructor */
Squad::Squad() 
    : _count(0), _unit(0) {}

/* Destructor */
Squad::~Squad()
{
    if (this->_unit)
    {
        for (int i = 0 ; i < this->_count ; i++)
            delete this->_unit[i];
        delete[] this->_unit;
        this->_unit = 0;
    }
}

/* Copy constructor */
Squad::Squad(const Squad& copyObj)
{
    this->_count = copyObj.getCount();
 
    // Create new tab and copy all tab in
    this->_unit = new ISpaceMarine* [this->_count];
    for (int i = 0 ; i < this->_count ; i++)
        this->_unit[i] = copyObj._unit[i]->clone();
}

/* Overloading operator */
Squad& Squad::operator = (const Squad& assignObj) 
{
    this->_count = assignObj.getCount();
    if (this->_unit)
    {
        // delete unit if exist
        for (int i = 0 ; i < this->_count ; i++)
            delete this->_unit[i];
        delete[] this->_unit;
        this->_unit = 0;
    }
    // copy unit
    this->_unit = new ISpaceMarine* [this->_count];
    for (int i = 0 ; i < this->_count ; i++)
        this->_unit[i] = assignObj._unit[i]->clone();
    return (*this);
}

/* Method */

int
Squad::getCount() const
{
    return (this->_count);
}

int
Squad::push(ISpaceMarine* newUnit)
{
    ISpaceMarine** tmp = this->_unit;

    int size = this->getCount() + 1;
    
    // delete this->_unit;
    this->_unit = new ISpaceMarine*[size];

    // copying the previous array + new
    for (int i = 0 ; i < size - 1 ; i++)
        this->_unit[i] = tmp[i];
    this->_unit[size - 1] = newUnit;

    this->_count++;
    delete[] tmp;
    return (this->_count);
}

ISpaceMarine* Squad::getUnit(int n) const
{
    if (n > this->_count || n < 0)
        return (0);
    return (this->_unit[n]);
}