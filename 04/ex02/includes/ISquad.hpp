#ifndef ISQUAD_HPP
#define ISQUAD_HPP

#include "ISpaceMarine.hpp"

class ISquad
{
       public:
          /* Desconstructor */
          virtual ~ISquad() {}
          /* Methods */
          virtual int getCount() const = 0;
          virtual int push(ISpaceMarine* newUnit) = 0;
          virtual ISpaceMarine* getUnit(int i) const = 0;
};


#endif
