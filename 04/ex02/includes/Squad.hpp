#ifndef SQUAD_HPP
#define SQUAD_HPP

#include "ISquad.hpp"

class Squad : public ISquad
{
    public:
        /* Constructor */
            Squad();
        /* Destructor */
            virtual ~Squad();
        /* Copy constructor */
            Squad(const Squad& assignObj);
        /* Overloading operator */
            Squad& operator = (const Squad& assignObj);
        /* Method */
            virtual int getCount() const;
            virtual int push(ISpaceMarine* newUnit);
            virtual ISpaceMarine* getUnit(int n) const;
    private:
        /* Attributs */
            int             _count;
            ISpaceMarine**  _unit;

};

#endif
