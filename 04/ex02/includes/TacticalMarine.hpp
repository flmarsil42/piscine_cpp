#ifndef TACTICALMARINE_HPP
#define TACTICALMARINE_HPP

#include "ISpaceMarine.hpp"

#include <iostream>

class TacticalMarine : public ISpaceMarine
{
       public:
            /* Constructor */
            TacticalMarine();

            /* Destructor */
            virtual ~TacticalMarine();

            /* Methods */
            virtual TacticalMarine* clone() const;
            virtual void battleCry() const;
            virtual void rangedAttack() const;
            virtual void meleeAttack() const;
};

#endif