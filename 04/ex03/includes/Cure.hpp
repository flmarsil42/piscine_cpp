#ifndef CURE_HPP
#define CURE_HPP

#include <iostream>

#include "AMateria.hpp"

class Cure : public AMateria
{ 
    public:
        /* Constructor  */
            Cure(const std::string& type = "cure");
        /* Deconstructor  */
            virtual ~Cure();
        /* Copy constructor  */
            Cure(const Cure& copyObj);
        /* Overloading operator  */
		    Cure& operator=(const Cure& assignObj);
        /* Methods */
            virtual Cure* clone() const;
            virtual void use(ICharacter& target);
};

#endif
