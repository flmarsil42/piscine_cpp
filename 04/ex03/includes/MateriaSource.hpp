#ifndef MATERIASOURCE_HPP
#define MATERIASOURCE_HPP

#include "IMateriaSource.hpp" 

class MateriaSource : public IMateriaSource
{
       public:
            /* Constructor  */
                MateriaSource();
            /* Deconstructor  */
                virtual ~MateriaSource();
            /* Copy constructor  */
                MateriaSource (const MateriaSource & copyObj);
            /* Overloading operator  */
                MateriaSource & operator = (const MateriaSource & assignObj);
            /* Methods  */
                virtual void learnMateria(AMateria* m);
                virtual AMateria* createMateria(std::string const & type);
        
        private:
            /* Attributs */
                int         _nMaxMateria;
                int         _stock_space;
                AMateria*   _stock[4];
};

#endif
