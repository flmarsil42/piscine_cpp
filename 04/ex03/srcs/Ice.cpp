#include "../includes/Ice.hpp"

/* Constructor  */
Ice::Ice(const std::string& type)
    : AMateria(type) {}

/* Deconstructor  */
Ice::~Ice(){}

/* Copy constructor  */
Ice::Ice(const Ice& copyObj)
    : AMateria(copyObj) {}

/* Overloading operator  */
Ice& Ice::operator = (const Ice& assignObj)
{
    if (this == &assignObj)
		return (*this);
    this->_xp = assignObj.getXP();
    return (*this);
}

/* Methods  */
void
Ice::use(ICharacter& target)
{
    std::cout
        << "*shoots an ice bolt at "
        << target.getName()
        << "*"
        << std::endl;
    AMateria::use(target);
}

Ice* Ice::clone() const
{
    return (new Ice);
}