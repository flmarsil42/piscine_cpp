#include "../includes/AMateria.hpp"

/* Constructor  */
AMateria::AMateria(const std::string& type)
    : _xp(0), _type(type) {}

/* Deconstructor  */
AMateria::~AMateria(){}

/* Copy constructor  */
AMateria::AMateria(const AMateria& copyObj)
    : _xp(copyObj.getXP()), _type(copyObj.getType()) {}

/* Overloading operator  */
AMateria& AMateria::operator = (const AMateria& assignObj)
{
    if (this == &assignObj)
		return (*this);
    this->_xp = assignObj.getXP();
    return (*this);
}

/* Methods  */
void
AMateria::use(ICharacter& target)
{
    (void)target;
    this->_xp += 10;
}

unsigned int
AMateria::getXP() const
{
    return (this->_xp);
}

std::string const&
AMateria::getType() const
{
    return (this->_type);
}
