#ifndef FIXED_CLASS_HPP
#define FIXED_CLASS_HPP

#include <iostream>

class Fixed {

	public:

		Fixed(); 								// constructeur par defaut
		~ Fixed();								// destructeur
		Fixed(const Fixed& stockRef);			// constructeur de copie
		Fixed& operator=(const Fixed& copyRef); // opérateur d'assignation

		int 				getRawBits(void) const;
		void 				setRawBits(int const raw);

	private:

		int					_stock;		// stock la valeur a point fixe
		int static const	_nbits = 8;	// stocker le nombre de bits, toujours = à 8.
};

#endif
