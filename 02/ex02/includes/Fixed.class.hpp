#ifndef FIXED_CLASS_HPP
#define FIXED_CLASS_HPP

#include <iostream>
#include <math.h>

class Fixed {

	public:

		// constructeurs - déconstructeurs
		Fixed(); 								// constructeur par defaut
		~ Fixed();								// destructeur
		Fixed(const Fixed& stockRef);			// constructeur de copie
		Fixed(const int n); 					// constructeur qui converti la valeur (int)n en valeur fixe
		Fixed(const float n);					// constructeur qui converti la valeur (float)n en valeur fixe
	
		// opérateurs arithmetiques
		Fixed&	operator=(const Fixed& copyRef);
		Fixed	operator+(const Fixed& copyRef) const;
		Fixed	operator-(const Fixed& copyRef) const;
		Fixed	operator*(const Fixed& copyRef) const;
		Fixed	operator/(const Fixed& copyRef) const;
	
		// opérateurs d'incrémentation - décrémentation
		Fixed 	&operator++(void);
		Fixed 	&operator--(void);
		Fixed 	operator++(int);				// (int) = syntax pour post incrémentation
		Fixed 	operator--(int);				// (int) = syntax pour post incrémentation
	
		// opérateurs de comparaison
		bool	operator<(const Fixed& copyRef) const;
		bool	operator>=(const Fixed& copyRef) const;
		bool	operator<=(const Fixed& copyRef) const;
		bool	operator==(const Fixed& copyRef) const;
		bool	operator!=(const Fixed& copyRef) const;
	
		// méthodes
		int 	getRawBits(void) const;
		void 	setRawBits(int const raw);
		float 	toFloat(void) const;
		int 	toInt(void) const;

		// fonctions non-membre static
		static const Fixed& min(Fixed const &a, Fixed const &b);
		static Fixed&       min(Fixed &a, Fixed &b);       
		static const Fixed& max(Fixed const &a, Fixed const &b);       
		static Fixed&       max(Fixed &a, Fixed &b);  

	private:

		int					_stock;		// stock la valeur a point fixe
		int static const	_nbits = 8;	// stocker le nombre de bits, toujours = à 8.

};
	
std::ostream& operator<<(std::ostream& oStream, const Fixed& fixed);


#endif
