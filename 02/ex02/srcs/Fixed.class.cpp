#include "../includes/Fixed.class.hpp"

//---------------------------------------------------
// 			CONSTRUCTEURS/DECONSTRUCTEURS 			|
//---------------------------------------------------

// constructeur par defaut
Fixed::Fixed() : _stock(0)
{
	std::cout << "Default constructor called\n";
	return ;
}

// constructeur de copie
Fixed::Fixed(const Fixed& stockRef)
{
	std::cout << "Copy constructor called\n";
	this->_stock = stockRef.getRawBits();
	return ;
} 

// constructeur qui converti un int en nombre fixe
Fixed::Fixed(const int n)
{
	std::cout << "Int constructor called\n";
	this->_stock = n << this->_nbits;
	return ;
}
// constructeur qui converti un float en nombre fixe
Fixed::Fixed(const float n)
{
	std::cout << "Float constructor called\n";
	this->_stock  = (int)(roundf(n * (1 << this->_nbits)));
	return ;
}

// destructeur
Fixed::~ Fixed()
{
	std::cout << "Destructor called\n";
	return ;
}

//---------------------------------------------------
// 						METHODES					|
//---------------------------------------------------

// initialise la valeur à point fixe
void
Fixed::setRawBits(int const raw)
{
	std::cout << "setRawBits member function called\n";
	this->_stock = raw;
	return ;
}

// retourne la valeur à point fixe
int
Fixed::getRawBits(void) const
{
	std::cout << "getRawBits member function called\n";
	return (this->_stock);
}

// Retourne la conversion fixe en int
int Fixed::toInt() const
{
	return (this->_stock >> this->_nbits);
}

// Retourne la conversion fixe en float
float Fixed::toFloat() const
{
	return ((float)this->_stock / (float)(1 << this->_nbits));
}

//---------------------------------------------------
// 			    OPERATEURS D'ASSIGNATION 			|
//---------------------------------------------------

Fixed&
Fixed::operator=(const Fixed& copyRef){
	std::cout << "Assignation operator called\n";
	this->_stock = copyRef.getRawBits();
	return (*this);
}

// Output d'un nombre fixe en float
std::ostream& operator<<(std::ostream& output, const Fixed& fixed)
{
	return (output << fixed.toFloat());
}

// Output addition
Fixed
Fixed::operator+(const Fixed& copyRef) const
{
	return (this->toFloat() + copyRef.toFloat());
}

// Output soustraction
Fixed
Fixed::operator-(const Fixed& copyRef) const
{
	return (this->toFloat() - copyRef.toFloat());
}

// Output multiplication
Fixed
Fixed::operator*(const Fixed& copyRef) const
{
	return (this->toFloat() * copyRef.toFloat());
}

// Output division
Fixed
Fixed::operator/(const Fixed& copyRef) const
{
	return (this->toFloat() / copyRef.toFloat());
}

// opérateurs de comparaison
bool
Fixed::operator<(const Fixed& copyRef) const {
	return (this->toFloat() < copyRef.toFloat());
}

bool
Fixed::operator>=(const Fixed& copyRef) const {
	return (this->toFloat() >= copyRef.toFloat());
}

bool
Fixed::operator<=(const Fixed& copyRef) const {
	return (this->toFloat() <= copyRef.toFloat());
}

bool
Fixed::operator==(const Fixed& copyRef) const {
	return (this->toFloat() == copyRef.toFloat());
}

bool
Fixed::operator!=(const Fixed& copyRef) const {
	return (this->toFloat() != copyRef.toFloat());
}

Fixed&
Fixed::operator++(void)
{
	this->_stock++;
	return (*this);
}

Fixed&
Fixed::operator--(void)
{
	this->_stock--;
	return (*this);
}

Fixed
Fixed::operator++(int)
{
	Fixed	tmp;

	tmp.setRawBits(this->_stock);
	this->_stock++;
	return (tmp);
}

Fixed
Fixed::operator--(int)
{
	Fixed	tmp;

	tmp.setRawBits(this->_stock);
	this->_stock--;
	return (tmp);
}


//---------------------------------------------------
// 			    FONCTIONS NON MEMBRES 				|
//---------------------------------------------------

const Fixed&
Fixed::min(Fixed const& a, Fixed const& b){
	return ((a >= b) ? b : a);
}

Fixed&
Fixed::min(Fixed& a, Fixed& b){
	return ((a >= b) ? b : a);
}

const Fixed&
Fixed::max(Fixed const& a, Fixed const& b){
	return ((a >= b) ? a : b);
}

Fixed&
Fixed::max(Fixed& a, Fixed& b){
	return ((a >= b) ? a : b);
}
