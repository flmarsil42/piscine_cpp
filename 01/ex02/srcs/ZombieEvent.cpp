/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/25 11:56:16 by a42               #+#    #+#             */
/*   Updated: 2021/01/03 14:08:56 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ZombieEvent.hpp"

// //constructeur par default
// ZombieEvent::ZombieEvent(){
// 	this->_listNames[0] = "Pierre";
// 	this->_listNames[1] = "David";
// 	this->_listNames[2] = "Jean";
// 	this->_listNames[3] = "Daniel";
// 	this->_listNames[4] = "Judas";
// 	return;
// }

//constructeur non par default
ZombieEvent::ZombieEvent() : _type("Zombie heap"){
	this->_listNames[0] = "Pierre";
	this->_listNames[1] = "David";
	this->_listNames[2] = "Jean";
	this->_listNames[3] = "Daniel";
	this->_listNames[4] = "Judas";
	std::cout << "\033[36mConstructeur Event : Zombie (\033[m" << this << "\033[36m) : construit\033[m" << std::endl;
	return;
}

//destructeur
ZombieEvent::~ ZombieEvent(){
	std::cout << "\033[31mDestructeur : Zombie (\033[m" << this << "\033[31m) : détruit\033[m" << std::endl;
	return ;
}

//ajoute le type du zombie dans l'objet
void
ZombieEvent::setZombieType(std::string type){
	this->_type = type;
	return ;
}

//creait un nouveau zombie avec un nouveau nom et le type 
Zombie*
ZombieEvent::newZombie(std::string name){
	Zombie* newZombie;
	newZombie = new Zombie(name, _type);
	return (newZombie);
}

//creait un nouveau zombie et genere un nombre aleatoire pour definir un nom aleatoire
Zombie*
ZombieEvent::randomChump() const{
	srand (time(NULL));
	int 	random = rand() % 5;
	Zombie* newZombie;

	newZombie = new Zombie(this->_listNames[random], _type);
	newZombie->advert();
	return (newZombie);
}
