/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/24 16:22:41 by a42               #+#    #+#             */
/*   Updated: 2021/01/06 16:30:10 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ZOMBIE_HPP
#define ZOMBIE_HPP

#include <iostream>
#include <algorithm>

class Zombie{

	public:
	
		Zombie();
		~ Zombie();
		
		Zombie(std::string name, std::string type);
		void 		advert() const;

	private:
		std::string _name;
		std::string _type;

};

#endif