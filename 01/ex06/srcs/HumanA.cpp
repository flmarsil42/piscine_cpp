#include "../includes/Weapon.hpp"
#include "../includes/HumanA.hpp"

HumanA::HumanA(std::string name, const Weapon& type) : _name(name), _weapon(type){}

HumanA::~ HumanA(){}

void
HumanA::attack(){
	std::cout << this->_name << " attacks with his " << this->_weapon.getType() << std::endl;
	return ;
}
