#include "../includes/Weapon.hpp"
#include "../includes/HumanB.hpp"

HumanB::HumanB(std::string name) : _name(name), _weapon(0){}

HumanB::~ HumanB(){}

//Sets a new weapon (ptr pointing to type)
void
HumanB::setWeapon(Weapon& type){
	this->_weapon = &type;
}

void
HumanB::attack(){
	std::cout << this->_name << " attacks with his " << this->_weapon->getType() << std::endl;
	return ;
}
