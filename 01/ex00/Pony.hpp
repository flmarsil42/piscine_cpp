/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/22 17:56:02 by a42               #+#    #+#             */
/*   Updated: 2021/01/03 13:43:25 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_HPP
#define PONY_HPP

#include <iostream>
#include <cstdlib>

class Pony {

public:
	Pony();
	~ Pony();
		
	void	ponyOnTheStack();
	void	ponyOnTheHeap();
	int 	exist;

private:
	int		_kmh;
};

#endif