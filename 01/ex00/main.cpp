/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/22 18:59:38 by a42               #+#    #+#             */
/*   Updated: 2021/01/03 13:42:41 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"


void	pony_stack_creation()
{
	std::cout << "\033[32m\n\nCréation de Pony Stack\033[m" << std::endl;
	Pony stack;
	stack.ponyOnTheStack();
	std::cout << "\n\n";

	std::cout << "\033[33m\nTest existence Pony Stack :\033[m" << std::endl;
	if (stack.exist)
		std::cout << "Pony Stack existe encore\n\n" << std::endl;
	else
		std::cout << "Pony Stack n'existe plus\n\n" << std::endl;
	return ;
}

void 	pony_heap_creation()
{
	std::cout << "\033[32m\n\nCréation de Pony Heap\033[m" << std::endl;
	Pony *heap = new Pony;
	heap->ponyOnTheHeap();
	std::cout << "\033[33m\nTest existence Pony Heap :\033[m" << std::endl;
	if (heap->exist)
		std::cout << "Pony Heap existe encore\n\n" << std::endl;
	else
		std::cout << "Pony Stack n'existe plus\n\n" << std::endl;
	return ;
}

int		main(void)
{
	std::string 	buf;
	int 			test = 2;

	while (test > 0)
	{
		std::cout << "---------------------------------------------" << std::endl;
		std::cout << "         LANCE LE PONY DE TON CHOIX" << std::endl;
		std::cout << "---------------------------------------------" << std::endl;
		std::cout << "1 - 🎠 Pony Stack 🎠 \n2 - 🏇 Pony Heap 🏇" << std::endl;
		std::cout << "$";
		std::getline(std::cin, buf);
		// buf.erase(std::remove_if(buf.begin(), buf.end(), ::isspace), buf.end());
		if (atoi(buf.c_str()) == 1)
		{
			pony_stack_creation();
			test--;
		}
		else
		{
			pony_heap_creation();
			test--;
		}
	}
	return (0);
}