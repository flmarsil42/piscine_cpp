/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/24 16:12:35 by a42               #+#    #+#             */
/*   Updated: 2021/01/03 13:45:44 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

void memoryLeak() {
	std::string 	*panthere = new std::string("String panthere");

	std::cout << *panthere << std::endl;
	delete panthere;
	// panthere = nullptr;
}

int		main(){
	memoryLeak();
	system("leaks a.out");
	return (0);
}