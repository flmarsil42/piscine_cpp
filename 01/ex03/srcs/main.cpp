/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: a42 <a42@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/25 11:33:58 by a42               #+#    #+#             */
/*   Updated: 2021/01/03 14:13:02 by a42              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/Zombie.hpp"
#include "../includes/ZombieHorde.hpp"

int		main()
{
	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "       Création d'une horde de 5 zombies"                  << std::endl;
	std::cout << "---------------------------------------------" << std::endl;
	ZombieHorde Horde1(5);
	Horde1.announce();

	std::cout << "---------------------------------------------" << std::endl;
	std::cout << "       Création d'une horde de 10 zombies"                  << std::endl;
	std::cout << "---------------------------------------------" << std::endl;
	ZombieHorde Horde2(10);
	Horde2.announce();
	system("leaks Zombie");
	return (0);
}
