#include "../includes/ZombieHorde.hpp"

ZombieHorde::ZombieHorde(unsigned int n) : _nZomb(n){
	this->_listNames[0] = "Pierre";
	this->_listNames[1] = "David";
	this->_listNames[2] = "Jean";
	this->_listNames[3] = "Daniel";
	this->_listNames[4] = "Judas";

	this->_tabZomb = new Zombie[n];
	srand(time(NULL));
	while (n)
		_tabZomb[--n].setName(_listNames[rand() % 5]);
}

ZombieHorde::~ ZombieHorde(){
	delete [] _tabZomb;
	// _tabZomb = nullptr;
}

//affiche non et type du zombie
void
ZombieHorde::announce() const{
	for (int i = 0 ; i < _nZomb; i++)
		_tabZomb[i].advert();
}
